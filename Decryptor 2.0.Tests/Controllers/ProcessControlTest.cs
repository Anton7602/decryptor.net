﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Decryptor;
using Decryptor.Controllers;

namespace Decryptor.Tests.Controllers
{
    [TestClass]
    public class ProcessControlTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            ProcessController controller = new ProcessController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
