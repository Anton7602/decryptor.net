﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Decryptor;
using Decryptor.Controllers;
using Decryptor.Models;

namespace Decryptor.Tests.Controllers
{
    [TestClass]
    public class AlphabetControllerTest
    {
        [TestMethod]
        public void FormAlphabet1()
        {
            {
                // Arrange
                AlphabetController controller = new AlphabetController();

                // Act
                controller.FormAlphabet(true,true,true,true,true,false,"");

                // Assert
                Assert.AreEqual(Processor.EncryptionAlphabet, $@"абвгдеёжзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyz" +
                    $@"0123456789!#$%&'()*+,-./:;<=>?[\^_`|~ ");
            }
        }
        [TestMethod]
        public void FormAlphabet2()
        {
            {
                // Arrange
                AlphabetController controller = new AlphabetController();

                // Act
                controller.FormAlphabet(true, false, true, false, false, true, "£");

                // Assert
                Assert.AreEqual(Processor.EncryptionAlphabet, $@"абвгдеёжзийклмнопрстуфхцчшщъыьэюя" +
                    $@"0123456789£");
            }
        }
        [TestMethod]
        public void FormAlphabet3()
        {
            {
                // Arrange
                AlphabetController controller = new AlphabetController();

                // Act
                controller.FormAlphabet(false, false, false, false, false, false, "£");

                // Assert
                Assert.AreEqual(Processor.EncryptionAlphabet, "");
            }
        }
        [TestMethod]
        public void FormAlphabet4()
        {
            {
                // Arrange
                AlphabetController controller = new AlphabetController();

                // Act
                controller.FormAlphabet(true, false, false, false, false, true, "абвгдийуфхцчьэю£ийуф");

                // Assert
                Assert.AreEqual(Processor.EncryptionAlphabet, "абвгдеёжзийклмнопрстуфхцчшщъыьэюя£");
            }
        }
    }
}
