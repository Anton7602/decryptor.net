﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Decryptor;
using Decryptor.Controllers;
using Decryptor.Models;

namespace Decryptor.Tests.Controllers
{
    [TestClass]
    public class ProcessorModelTest
    {
        [TestMethod]
        public void Decode()
        {
            // Arrange
            Processor.InputText = "бщцфаирщри, бл ячъбиуъ щбюэсяёш гфуаа!!!";
            Processor.EncryptionAlphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            Processor.CurrentOperation = Operation.Decrypt;
            Processor.KeyWord = "Скорпион";

            // Act
            Processor.DoOperation();

            // Assert
            Assert.AreEqual(Processor.OutputText, "поздравляю, ты получил исходный текст!!!");
        }
        [TestMethod]
        public void Encode()
        {
            // Arrange
            Processor.InputText = "в принципе понять, что тут используется шифр виженера не особо трудно";
            Processor.EncryptionAlphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            Processor.CurrentOperation = Operation.Encrypt;
            Processor.KeyWord = "Скорпион";

            // Act
            Processor.DoOperation();

            // Assert
            Assert.AreEqual(Processor.OutputText, "у ъящэячэц ъэюоык, едщ бдв саэацкшгнбяр гчеа кчфцшубп цу ьгщпя вщвсящ");
        }
    }
}
